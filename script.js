console.log("** JS ARRAYS **");
/*Arrays - used to store multiple related values in a single variable
		 - declared using square breackets [] known as array literals


Elements- are values inside an array
index - 0 = offset
element 1 is in the index 0

Syntax: let/const arrayName = [elementA,elementB,elementC]
*/

let grades = [98,94,89,90];
let computerBrands = ['Acer','Asus','Lenovo','Neo',"Redfox",'Gateway','Toshiba','Fujitsu'];
let mixedArr = [12,'Asus', null , undefined, {}]; // Not recomended

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[10])

let myTasks = [
	'bake sass',
	'drink html',
	'inhale css',
	'eat javascript',
];

console.log(myTasks);

//Reassigning value

myTasks[0] = 'hello world';
console.log(myTasks)

//Getting the length of an array
console.log(computerBrands.length);

// let lastIndex = myTasks.length -1;
// console.log(lastIndex)

/*
ARRAY METHODS
1. Mutator Methods
	-are functions that "mutate" or change an array
*/

//push - adds an element in the end of the array and reutrn its length
// syntax: arrayName.push(elementA, elementB)

let fruits = ['Apple','Orange','Kiwi','DragonFruit'];
console.log('Current array');
console.log(fruits);
console.log('Array after push()');

let fruitsLength = fruits.push('Mango');
console.log('Mutated array after push()');
console.log(fruits);
console.log(fruitsLength);

fruits.push('Guava','Avocado');
console.log('Mutated array after push()');
console.log(fruits);

//pop -removes the last element in an array and return the removed element

let removedFruit = fruits.pop();
console.log('Mutated array after pop()');
console.log(fruits);
console.log(removedFruit);

//shift - removes an element at the beginning of an array and return the removed element

let firstFruit = fruits.shift();
console.log('Mutated array after shift()');
console.log(fruits);
console.log(firstFruit);

//unshift - adds one or more element/s at the beginning of an array

fruits.unshift('Lime','Banana')
console.log('Mutated array after unshift()');
console.log(fruits);

//splice - removes element from specified index and add new elements
//syntax; arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

fruits.splice(1);
console.log(fruits);
fruits.splice(2,2);
console.log(fruits);
fruits.splice(1,2,'Cherry','Grapes');
console.log('Mutated array after splice()');
console.log(fruits);

//sort - rearranges the elements in alphanumeric order
fruits.sort();
console.log('Mutated array after sort()');
console.log(fruits);

//reverse -reverses the order of an array
fruits.reverse();
console.log('Mutated array after reverse()');
console.log(fruits);

/*
2. Non Mutator Methods
	-are functions that do not modify or change the array*\*/

let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

//indexOf() 
//-returns the first index of the first matching element found in an array (left to right)
//if no match is found, it returns -1

let firstIndex = countries.indexOf('PH');
console.log('Result of index() method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of index() method: ' + invalidCountry);

//lastIndexOf()
//-returns the last matching element found in an array (right to left)
//syntax: arrayName.lastIndexOf(searchValue)
//arrayName.lastIndexOf(searchValue, fromIndex);

let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf() method: ' + lastIndex);

let lastIndexStart = countries.lastIndexOf('PH',2);
console.log('Result of lastIndexOf() method: ' + lastIndexStart);

//slice - slices a portion of an array and return a new array
//syntax arrayName.slice(startingIndex);
//syntax arrayName.slice(startingIndex, endingIndex);

let slicedArrayA = countries.slice(2);
console.log('Result from slice method A');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,6);
console.log('Result from slice method B');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method C');
console.log(slicedArrayC);

//toString

let stringArray = countries.toString();
console.log('Result from toString()');
console.log(stringArray);

//concat - combines two or more arrays and return the combined result

let tasksArrayA = ['drink HTML','eat javascript'];
let tasksArrayB = ['inhale CSS','breathe sass'];
let tasksArrayC = ['get git','be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat()');
console.log(tasks);

let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result from concat()');
console.log(allTasks);

let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat()');
console.log(combinedTasks);

//join - returns an array as string separated by specified separator

let users = ['John','Jane','Joe','Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' & '));

/*3. Iteration Methods
	-loops designed to perform repetitive tasks on arrays*/

//foreach
//syntax:
/*arrayName.forEach(function(individualElement){

})*/

countries.forEach(function(country){
	console.log(country);
})

allTasks.forEach(function(task){
	console.log(task);
})

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length>10){
		filteredTasks.push(task);
	}
})

console.log('Result from forEach()');
console.log(filteredTasks);

//map - iterates on each element and returns new array with different values depending on the results of the function's operation

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number){
	return number * number;
})

console.log('Result from map()');
console.log(numberMap)

let numberMap2 = [];
numbers.forEach(function(number){
	let square = number * number;
	numberMap2.push(square);
})
console.log('Result from forEach()')
console.log(numberMap2);

let arrayMap = allTasks.map(function(task){
	return 'My' + task;
})

console.log('Result from arrayMap()');
console.log(arrayMap);

//every 

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log('Result from every ()');
console.log(allValid);

//some

let someValid = numbers.some(function(number){
	return (number < 3);
})

console.log('Result from some()');
console.log(someValid);

//filter - returns a new array that meets the given condition

let filterValid = numbers.filter(function(number){
	return (number <3);
})

console.log('Result from filter ()');
console.log(filterValid);

//includes 
let products = ['Mouse','Keyboard','Laptop','Monitor']

let filterProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
})

console.log('Result from filter() & includes');
console.log(filterProducts);

//reduce
/*let/const resultArray = arrayName.reduce(function(accumulator, current value){
	statement
})*/

//Multi-dimensional Array
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);
console.log(chessBoard[1][4]) //e2
console.log('Pawn moves to f: ' + chessBoard[1][5]);
console.log('Pawn moves to c7'+ chessBoard[6][2])